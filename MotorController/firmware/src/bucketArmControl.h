/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _BUCKET_ARM_CONTROL_H    /* Guard against multiple inclusion */
#define _BUCKET_ARM_CONTROL_H


void moveBucketArmToDig(void);
void moveBucketArmToHomeFromDig(void);
void moveBucketArmToHomeFromDump(void);
void moveBucketArmToDump(void);
void zeroBucketArm(void);
void bucketArmResetPositionData(void);
void moveBucketArmToDig2(void);
#endif /* _BUCKET_ARM_CONTROL_H */

/* *****************************************************************************
 End of File
 */
