/* 
 * File:   Definitions.h
 * Author: John
 *
 * Created on April 27, 2018, 6:32 PM
 */

#ifndef DEFINITIONS_H
#define	DEFINITIONS_H


#define LED1 LATEbits.LATE5
#define LED2 LATEbits.LATE6
#define LED3 LATEbits.LATE7
#define LED4 LATEbits.LATE4

#define MY_ADDRESS      6 //THE FILTER WILL BE 0x7fc1 (Address 1)

#define MASTER_ADDRESS 5

#define MACRO_COMMAND_INDEX 1
#define MACRO_COMMAND_DATA_INDEX 2


#endif	/* DEFINITIONS_H */

