/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _DIGGING_MACROS_H    /* Guard against multiple inclusion */
#define _DIGGING_MACROS_H
#include <stdbool.h>
void plowMacro(void);
void diggingMacro(bool b);

void diggingMacro2(bool b);

#endif /* _DIGGING_MACROS_H */

/* *****************************************************************************
 End of File
 */
